<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>Document</title>
</head>
<body>
<?php
// define variables and set to empty values
$name = $visib = $file = "";
$nameErr = $visibErr = $fileErr = "";

if (isset($_GET['name'])) { $name=$_GET['name']; }
if (isset($_GET['visib'])) { $visib=$_GET['visib']; }
//if (isset($_FILES['file'])) { $file=$_FILES['file']; }

if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
if (isset($_GET['visibErr'])) { $visibErr = $_GET['visibErr']; }
//if (isset($_FILES['fileErr'])) { $fileErr = $_FILES['fileErr']; }
?>
<form method="post" name="upload" action="myPhoto.php" enctype="multipart/form-data" >

<div>
  <label for="name">User name <span class="required">* <?php echo $nameErr;?></span></label><br>
  <input type="text" name="name" value="<?php echo $name;?>">
</div>

<div>
<label for="if">Picture <span class="required">* <?php echo $fileErr;?></span></label><br>
<input type="file" name="file" value="<?php echo $file;?>"><br />

    <div>
        <label for="accesslev">Visibility <span class="required">* <?php echo $visibErr;?></span></label><br>
        <input class="accesslev" type="radio" name="visib" <?php if (isset($visib) && $visib=="private") echo "checked";?> value="private">Private
        <input class="accesslev" type="radio" name="visib" <?php if (isset($visib) && $visib=="public") echo "checked";?> value="public">Public<br>
    </div>

<input type="submit" name="sb" class="button" value="upload" />
<input type="reset" name="rb" class="button" value="cancel" />
</div>

</form>

</body>
</html>