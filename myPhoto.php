<?php

// define variables and set to empty values
$name = $visib = $file = "";
$nameErr = $visibErr = $fileErr = "";

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and numbers
    if (!preg_match("/^[a-zA-Z-0-9]*$/",$name)) {
      $nameErr = "Only letters and numbers allowed"; 
      }
    }

  if (empty($_POST["visib"])) {
    $visibErr = "You must select an option";
  } else {
    $visib = test_input($_POST["visib"]);
  }

  /*if (empty($_POST["file"])) {
    $fileErr = "Must select picture file";
  } else {
    $file = test_input($_POST["file"]);
  }*/

  if (!empty($nameErr) or !empty($visibErr) or !empty($fileErr)) {
    $params = "name=" . urlencode($_POST["name"]);
    $params .= "&visib=" . urlencode($_POST["visib"]);
    $params .= "&file=" . urlencode($_POST["file"]);

    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&visibErr=" . urlencode($visibErr);
    $params .= "&fileErr" . urlencode($fileErr);

    header("Location: index.php?" . $params);
  }  
  else {
    echo "<h3>Picture info:</h3>";
    echo "Posted by: " .$_POST['name'];
    echo "<br>";
    echo "Visibility: " .$_POST['visib'];
    echo "<br>";
  }

  //var_dump($_FILES);

  if(isset($_FILES["file"]) AND is_uploaded_file($_FILES['file']['tmp_name'])) {
    $file_name = $_FILES['file']["name"];
    $file_temp = $_FILES["file"]["tmp_name"];
    $file_size = $_FILES["file"]["size"];
    $file_type = $_FILES["file"]["type"];
    $file_error = $_FILES['file']["error"];

    if ($file_error >0) {
      echo "Something went wrong during file upload!";
    } else {
        // http://en.wikipedia.org/wiki/Exchangeable_image_file_format
        // http://www.php.net/manual/en/book.exif.php
        echo "Image type integer: ".exif_imagetype($file_temp)."<br />";
        if (!exif_imagetype($file_temp)) {;
          exit("File is not a picture!");
        }
        elseif (exif_imagetype($file_temp)!=2) {
          $fileErr = "Must be jpg";
          exit("Picture is not a jpg");
        }
      }

    echo "Ime poslate datoteke: $file_name <br />";
    echo "Privremena lokacija datoteke: $file_temp <br />";
    echo "Veličina poslate datoteke u bajtovima: $file_size <br />";
    echo "Tip poslate datoteke: $file_type <br />";
    echo "Kod greške: $file_error <br />";

    $ext_temp = explode(".", $file_name);
    $extension = end($ext_temp);
    $date = date_create();
    $datestamp = date_timestamp_get($date);
    $rand = rand(1,10);

    $new_file_name = "$name" . "-" . "$datestamp" . "-" . "$rand" . ".$extension";

    $directory1 = "private";
    $directory2 = "public";

    // upload fajla

    if ($visib=="private" && !is_dir($directory1)) {
      mkdir($directory1);
      move_uploaded_file($file_temp, $directory1.'/'.$new_file_name);
    }
      elseif ($visib=="private" && is_dir($directory1)) {
        move_uploaded_file($file_temp, $directory1.'/'.$new_file_name);
    } else {
        if ($visib=="public" && !is_dir($directory2)) {
          mkdir($directory2);
          move_uploaded_file($file_temp, $directory2.'/'.$new_file_name);
        } else  {
          move_uploaded_file($file_temp, $directory2.'/'.$new_file_name);
          }
      }
    echo "Novo ime poslate datoteke: $new_file_name";
  }
}
?>